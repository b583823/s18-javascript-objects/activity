// [Trainer]

let trainor = {
	name:"",
	age: null,
	pokemon: [],
	friends:{
		hoenn:[],
		kanto:[],
	}
}



let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn:["May","Max"],
		kanto:["Brock","Misty"]
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu I choose you!")
	}

}
// console.log(trainor);

trainor.name = "Ash Ketchum";
console.log(trainor.name);
trainor.age=10;
// console.log(trainor.age);

trainor.friends = {
	hoenn: ["May","Max"],
	kanto: ["Brock", "Misty"]
};
console.log(trainor.friends);
trainor.pokemon = ["Pikachu, Charrizard", "Squirle", "Bulbasaur"];
console.log(trainor.pokemon);
trainer.talk()

// Pokemon Constructor

function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;
	// Method
	this.tackle = function(target){
					//pokemonObject			//targetPokemon
		console.log(this.name + " tackled "+target.name);
		console.log(target.name+"'s health is reduced to "+ (target.health - this.attack) );
		target.health -= this.attack;
		if(target.health<=0){
			target.faint();
		}
	} 
	this.faint = function(){
		console.log(this.name+ " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu",12);
console.log(pikachu);
let geodude = new Pokemon("Geodude",8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo",100);
console.log(mewtwo);

geodude.tackle(pikachu);
// pikachu.health -= geodude.attack;
console.log(pikachu);

mewtwo.tackle(geodude);
// target.faint();
console.log(geodude);